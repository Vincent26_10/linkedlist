
public class Stack {

	private LinkedList lin;

	public Stack() {
		lin = new LinkedList();
	}

	public void push(Object obj) {
		lin.insertFirst(obj);
	}

	public Object pop() throws EmptyListException, ObjectNotFoundException {
		Object ob = lin.getFirstObject();
		lin.remove(lin.getFirstObject());
		return ob;

	}

	public boolean isEmpty() {
		return lin.isEmpty();
	}

	public void empty() throws EmptyListException,ObjectNotFoundException {
		int val = 0;
		val = lin.getNumElements();
		for (int i = 0; i < val; i++) {

				lin.remove(lin.getFirstObject());

		}
	}

	public void Print() {
		lin.print();
	}

}
