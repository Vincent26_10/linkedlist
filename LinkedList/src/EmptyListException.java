
public class EmptyListException extends ObjectNotFoundException {
	public EmptyListException() {
		
		super("The list is empty");
	}
	public EmptyListException(String msg) {
		super(msg);
	}
}
