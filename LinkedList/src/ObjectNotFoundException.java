
public class ObjectNotFoundException extends LinkedListException {

	public ObjectNotFoundException() {
		super("Object not found");
	}
	
	public ObjectNotFoundException(String msg) {
		super(msg);
	}
}
