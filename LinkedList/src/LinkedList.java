
public class LinkedList {
	private Element first;
	private Element last;

	public LinkedList() {
		first = null;
		last = null;
	}

	public void insertFirst(Object obj) {
		Element e = new Element(obj);

		if (first == null) {
			last = e;
		} else {
			e.setNext(first);
		}
		first = e;
	}

	public void insertLast(Object obj) {
		Element e = new Element(obj);
		if (first == null) {
			first = e;
		} else {
			last.setNext(e);
		}
		last = e;

	}

	public void print() {
		Element e = first;
		while (e != null) {
			System.out.println(e.getObject());
			e = e.getNext();
		}

	}

	public boolean isEmpty() {
		if (first == null) {
			return true;
		} else {
			return false;
		}
	}

	public void remove(Object object) throws ObjectNotFoundException, EmptyListException {

		if (isEmpty()) {
			throw new EmptyListException();
		}

		Element current, previous;

		current = first;
		previous = first;

		while (current != null && !current.getObject().equals(object)) {
			previous = current;
			current = current.getNext();
		}
		if (current == null) {
			throw new ObjectNotFoundException("Element " + object + " not found in the list");
		}
		
		if (first == last) {
			first = null;
			last = null;
		} else {
			if (current == first) {
				first = current.getNext();
			} else {
				previous.setNext(current.getNext());
			}
			if (current == last) {
				last = previous;
			}
		}

	}

	public Object getFirstObject() throws EmptyListException {
		if (isEmpty()) {
			EmptyListException e = new EmptyListException("The list is empty");
			throw e;
		}
		return first.getObject();
	}

	public Object getLastObject() throws EmptyListException {
		if (isEmpty()) {
			EmptyListException e = new EmptyListException("The list is empty");
			throw e;
		}
		return last.getObject();
	}

	public int getNumElements() {
		Element e = first;
		int cont = 0;
		while (e != null) {
			cont++;
			e = e.getNext();
		}
		return cont;
	}

	public Object getObjectAtPosition(int i) throws EmptyListException, InvalidIndexException {
		if (isEmpty()) {
			EmptyListException e = new EmptyListException("The list is empty");
			throw e;
		}

		if (i < 0) {
			throw new InvalidIndexException();
		}
		Element e = first;
		int cont = 1;
		while (e != null) {
			if (cont == i) {
				return e.getObject();
			} else {
				cont++;
				e = e.getNext();
			}
		}
		if (cont <= i) {
			InvalidIndexException index = new InvalidIndexException("Invalid index");
			throw index;
		}

		throw new Error("This should never happend, if happends YOUR MON IS GAY");
	}
}
